Name:           ceph-ansible
Version:        1.0.1
Release:        4%{?dist}
Summary:        Ansible playbooks for Ceph
# Some files have been copied from Ansbile (GPLv3+). For example:
#  library/ceph_facts
#  plugins/actions/config_template.py
#  roles/ceph-common/plugins/actions/config_template.py
License:        ASL 2.0 and GPLv3+
URL:            https://github.com/ceph/ceph-ansible
Source0:        https://github.com/ceph/%{name}/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildArch:      noarch

BuildRequires: ansible
%if 0%{?fedora} || 0%{?rhel} > 7
BuildRequires: python3-devel
%else
BuildRequires: python2-devel
%endif

Requires: ansible

%description
Ansible playbooks for Ceph

%prep
%autosetup -p1

%build

%install
mkdir -p %{buildroot}%{_datarootdir}/ceph-ansible

for f in ansible.cfg *.yml *.sample group_vars roles library plugins; do
  cp -a $f %{buildroot}%{_datarootdir}/ceph-ansible
done

%check
# Borrowed from upstream's .travis.yml:
ansible-playbook -i dummy-ansible-hosts test.yml --syntax-check

%files
%doc README.md
%license LICENSE
%{_datarootdir}/ceph-ansible

%changelog
* Mon Mar 07 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.0.1-4
- BR: python{2,3}-devel (rhbz#1313477)

* Mon Mar 07 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.0.1-3
- Ship ansible.cfg

* Mon Mar 07 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.0.1-1
- Update to latest upstream release
- Correct License field (rhbz#1313477)

* Mon Feb 29 2016 Ken Dreyer <kdreyer@redhat.com> - 0-0.1.20160229gitf9c3159
- initial package
